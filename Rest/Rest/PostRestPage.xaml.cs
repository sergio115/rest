﻿using Newtonsoft.Json;
using Rest.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Rest
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostRestPage : ContentPage
    {
        private const string url = "http://172.20.10.4/webproductos/prod.php";
        private HttpClient _Client = new HttpClient();
        private ObservableCollection<Producto> _post;

        public PostRestPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            var content = await _Client.GetStringAsync(url);
            var post = JsonConvert.DeserializeObject<List<Producto>>(content);
            _post = new ObservableCollection<Producto>(post);
            lista.ItemsSource = _post;
            base.OnAppearing();
        }
    }
}