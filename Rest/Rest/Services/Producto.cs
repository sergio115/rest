﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rest.Services
{
    public class Producto
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Orden { get; set; }
        public int Contador { get; set; }
        public int Sscc { get; set; }
        public int Lote { get; set; }
    }
}
